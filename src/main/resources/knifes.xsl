<?xml version="1.0" encoding="UTF-8"?>

<xsl:stylesheet version="1.0"
                xmlns:xsl="http://www.w3.org/1999/XSL/Transform">

    <xsl:template match="/">
        <html>
            <body>
                <h2>Knifes</h2>
                <table border="1">
                    <tr bgcolor="#aaaaaa">
                        <th>id</th>
                        <th>type</th>
                        <th>handy</th>
                        <th>length</th>
                        <th>width</th>
                        <th>material</th>
                        <th>handle</th>
                        <th>bloodFlow</th>
                        <th>value</th>
                    </tr>
                    <xsl:for-each select="knifes/knife">
                        <xsl:sort select="origin"/>
                        <tr>
                            <td><xsl:value-of select="@id"/></td>
                            <td><xsl:value-of select="type"/></td>
                            <td><xsl:value-of select="handy"/></td>
                            <td><xsl:value-of select="origin"/></td>
                            <td><xsl:value-of select="visual/length"/></td>
                            <td><xsl:value-of select="visualParams/width"/></td>
                            <td><xsl:value-of select="visualParams/material"/></td>
                            <td><xsl:value-of select="visualParams/handle"/></td>
                            <td><xsl:value-of select="visualParams/bloodFlow"/></td>
                            <td><xsl:value-of select="value"/></td>
                        </tr>
                    </xsl:for-each>
                </table>
            </body>
        </html>
    </xsl:template>

</xsl:stylesheet>
