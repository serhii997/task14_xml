package com.movchan.parser.STAX;

import com.movchan.model.Knife;
import com.movchan.model.Visual;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class ParserStax {
    private static final Logger log = LogManager.getLogger(ParserStax.class);

    public List<Knife> parseToListByStAX(String path) throws XMLStreamException {
        List<Knife> gems = new ArrayList<>();

        Knife knife = new Knife();
        Visual visual = new Visual();

        File file = new File(path);
        XMLInputFactory factory = XMLInputFactory.newInstance();
        XMLStreamReader streamReader = null;
        try {
            streamReader = factory.createXMLStreamReader(new FileReader(file));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


        while (streamReader.hasNext()) {
            //Move to next event
            streamReader.next();

            //Check if its 'START_ELEMENT'
            if (streamReader.getEventType() == XMLStreamReader.START_ELEMENT) {
                //employee tag - opened
                if (streamReader.getLocalName().equalsIgnoreCase("knife")) {
                    //Create new employee object asap tag is open
                    knife = new Knife();
                    visual = new Visual();

                    //Read attributes within employee tag
                    if (streamReader.getAttributeCount() > 0) {
                        String id = streamReader.getAttributeValue(null, "id");
                        knife.setId(Integer.valueOf(id));
                    }
                } //Boolean.parseBoolean
                //Read name data
                if (streamReader.getLocalName().equalsIgnoreCase("type")) {
                    knife.setType(streamReader.getElementText());
                }
                //Read title data
                if (streamReader.getLocalName().equalsIgnoreCase("handy")) {
                    knife.setHandy(streamReader.getElementText());
                }
                if (streamReader.getLocalName().equalsIgnoreCase("origin")) {
                    knife.setOrigin(streamReader.getElementText());
                }
                if (streamReader.getLocalName().equalsIgnoreCase("value")) {
                    knife.setValue(Boolean.parseBoolean(streamReader.getElementText()));
                }
                if (streamReader.getLocalName().equalsIgnoreCase("length")) {
                    visual.setLength(Integer.parseInt(streamReader.getElementText()));
                }
                if (streamReader.getLocalName().equalsIgnoreCase("width")) {
                    visual.setWidth(Integer.parseInt(streamReader.getElementText()));
                }
                if (streamReader.getLocalName().equalsIgnoreCase("material")) {
                    visual.setMaterial(streamReader.getElementText());
                }
                if (streamReader.getLocalName().equalsIgnoreCase("handle")) {
                    visual.setHandle(streamReader.getElementText());
                }
                if (streamReader.getLocalName().equalsIgnoreCase("bloodFlow")) {
                    visual.setBloodFlow(Boolean.parseBoolean(streamReader.getElementText()));
                }
                knife.setVisual(visual);
            }

            //If employee tag is closed then add the employee object to list
            if (streamReader.getEventType() == XMLStreamReader.END_ELEMENT) {
                if (streamReader.getLocalName().equalsIgnoreCase("knife")) {
                    gems.add(knife);
                }
            }
        }


        return gems;
    }
}
