package com.movchan.parser.SAX;

import com.movchan.model.Knife;
import com.movchan.model.Visual;
import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import javax.xml.parsers.SAXParser;
import javax.xml.parsers.SAXParserFactory;
import java.util.ArrayList;
import java.util.List;

public class ParserSax {

    private static List<Knife> gems = new ArrayList<>();

    public List<Knife> parseToListBySAX(String path) {
        try {
            SAXParserFactory factory = SAXParserFactory.newInstance();
            SAXParser saxParser = factory.newSAXParser();

            MyHandler handler = new MyHandler();
            saxParser.parse(path, handler);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return gems;
    }

    private static class MyHandler extends DefaultHandler {
        String lastElementName;
        Integer id = 0;
        String type;
        String handy;
        String origin;

        Integer lengthKnife;
        Integer width;
        String material;
        String handle;
        Boolean bloodFlow;

        Boolean value;

        @Override
        public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
            if (qName.equals("knife")) {
                id = Integer.parseInt(attributes.getValue("id"));
            }
            lastElementName = qName;
        }

        @Override
        public void characters(char[] ch, int start, int length) throws SAXException {
            String information = new String(ch, start, length);
            information = information.replace("\n", "").trim();
            if (!information.isEmpty()) {
                if (lastElementName.equals("type")) {
                    type = information;
                }
                if (lastElementName.equals("handy")) {
                    handy = information;
                }
                if (lastElementName.equals("origin")) {
                    origin = information;
                }
                if (lastElementName.equals("value")) {
                    value = Boolean.parseBoolean(information);
                }
                if (lastElementName.equals("length")) {
                    lengthKnife = Integer.parseInt(information);
                }
                if (lastElementName.equals("width")) {
                    width = Integer.parseInt(information);
                }
                if (lastElementName.equals("material")) {
                    material = information;
                }
                if (lastElementName.equals("handle")) {
                    handle = information;
                }
                if (lastElementName.equals("bloodFlow")) {
                    bloodFlow = Boolean.parseBoolean(information);
                }
            }
        }

        @Override
        public void endElement(String uri, String localName, String qName) {
            if ((type != null && !type.isEmpty())
                    && (handy != null)
                    && (origin != null && !origin.isEmpty())
                    && (value != null)
                    && (material != null && !material.isEmpty())
                    && (handle != null && !handle.isEmpty())
                    && (lengthKnife != null)
                    && (width != null)
                    && (bloodFlow != null)) {
                gems.add(new Knife(id, type, handy, origin, new Visual(lengthKnife, width, material, handle, bloodFlow), value));
                id = null;
                type = null;
                handy = null;
                origin = null;
                value = null;
                material = null;
                handle = null;
                lengthKnife = null;
                width = null;
                bloodFlow = null;
            }
        }

    }
}
