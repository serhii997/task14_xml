package com.movchan.parser.DOM;

import com.movchan.model.Knife;
import com.movchan.model.Visual;
import jdk.internal.org.xml.sax.SAXException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ParseDom {
    private static final Logger log = LogManager.getLogger(ParseDom.class);
    public List<Knife> parseToListByDOM(String path) {
        List<Knife> knives = new ArrayList<>();
        try {
            NodeList nodeList = getNodeList(path);
            for (int temp = 0; temp < nodeList.getLength(); temp++) {
                Node node = nodeList.item(temp);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    knives.add(getKnife(element));

                }
            }
        } catch (ParserConfigurationException e) {
            log.error(e.getMessage());
        } catch (SAXException e) {
            log.error(e.getMessage());
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return knives;
    }

    private NodeList getNodeList(String path) throws ParserConfigurationException, SAXException, IOException, org.xml.sax.SAXException {
        File fXmlFile = new File(path);
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(fXmlFile);
        doc.getDocumentElement().normalize();
        return doc.getElementsByTagName("knife");
    }

    private Knife getKnife(Element elem) {
        Knife knife = new Knife();
        knife.setId(Integer.parseInt(elem.getAttribute("id")));
        knife.setType(elem.getElementsByTagName("type").item(0).getTextContent());
        knife.setHandy(elem.getElementsByTagName("handy").item(0).getTextContent());
        knife.setOrigin(elem.getElementsByTagName("origin").item(0).getTextContent());
        knife.setValue(Boolean.parseBoolean(elem.getElementsByTagName("value").item(0).getTextContent()));
        Visual visualParam = new Visual();
        visualParam.setLength(Integer.parseInt(elem.getElementsByTagName("length").item(0).getTextContent()));
        visualParam.setWidth(Integer.parseInt(elem.getElementsByTagName("width").item(0).getTextContent()));
        visualParam.setMaterial((elem.getElementsByTagName("material").item(0).getTextContent()));
        visualParam.setHandle((elem.getElementsByTagName("handle").item(0).getTextContent()));
        visualParam.setBloodFlow(Boolean.parseBoolean(elem.getElementsByTagName("bloodFlow").item(0).getTextContent()));
        knife.setVisual(visualParam);
        return knife;
    }
}
