package com.movchan.toJSON;

import com.google.gson.Gson;
import com.movchan.model.Knife;
import com.movchan.parser.STAX.ParserStax;

import javax.xml.stream.XMLStreamException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

public class POJOToJSON {
    public static void main(String[] args) throws XMLStreamException {
        Gson gson = new Gson();
        List<Knife> knifes = createStaffObject();


        try (FileWriter writer = new FileWriter("C:\\res\\knife.json")) {
            for (Knife knife : knifes) {
                gson.toJson(knife, writer);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static List<Knife> createStaffObject() throws XMLStreamException {
        final String path = "src/main/resources/knifes.xml";
        List<Knife> knifeList = new ParserStax().parseToListByStAX(path)
                .stream()
                .sorted(Comparator.comparing(Knife::getOrigin))
                .collect(Collectors.toList());
        return knifeList;
    }
}