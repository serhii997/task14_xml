package com.movchan.controller;

import com.movchan.model.Knife;
import com.movchan.parser.DOM.ParseDom;
import com.movchan.parser.SAX.ParserSax;
import com.movchan.parser.STAX.ParserStax;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.xml.stream.XMLStreamException;
import java.util.Comparator;

public class ControllerImp implements Controller {
    private static final Logger log = LogManager.getLogger(ControllerImp.class);
    @Override
    public void parseDOM(String path) {
        log.info("parseDOM");
        long startTime = System.nanoTime();
        new ParseDom().parseToListByDOM(path)
                .stream()
                .sorted(Comparator.comparing(Knife::getOrigin))
                .forEach(System.out::println);
        printTimeExecuting(startTime);
    }

    @Override
    public void parseSAX(String path) {
        log.info("parseSAX");
        long startTime = System.nanoTime();
        new ParserSax().parseToListBySAX(path)
                .stream()
                .sorted(Comparator.comparing(Knife::getOrigin))
                .forEach(log::info);
        printTimeExecuting(startTime);
    }

    @Override
    public void parseStAX(String path) {
        log.info("parseStAX");
        long startTime = System.nanoTime();
        try {
            new ParserStax().parseToListByStAX(path)
                    .stream()
                    .sorted(Comparator.comparing(Knife::getOrigin))
                    .forEach(log::info);
        } catch (XMLStreamException e) {
            e.printStackTrace();
        } finally {
            printTimeExecuting(startTime);
        }
    }
    private void printTimeExecuting(long startTime) {
        long endTime = System.nanoTime();
        long totalTime = endTime - startTime;
        log.info("Total time  - " + (double) totalTime / 1_000_000_000 + "sec.\n");
    }
}
