package com.movchan.controller;

public interface Controller {
    void parseDOM(String path);
    void parseSAX(String path);
    void parseStAX(String path);
}
